﻿using Microsoft.EntityFrameworkCore;

namespace DatabaseHomework
{
    public interface IMyDbContext
    {
        DbSet<Authors> Authors { get; set; }
        DbSet<Orders> Orders { get; set; }
    }
}