﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseHomework
{
    public class MyDbContext : DbContext, IMyDbContext
    {

        public MyDbContext(DbContextOptions options) : base(options) { }

        public DbSet<Authors> Authors { get; set; }
        public DbSet<Orders> Orders { get; set; }
    }
}
